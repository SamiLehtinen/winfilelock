WinFileLock
===========

Simple Python 3 file locking for Windows platform only.

This code is based on
[existing multi-platform implementation](https://github.com/ilastik/lazyflow/blob/master/lazyflow/utility/fileLock.py). 
Which is based on this older 
[implementation](http://www.evanfosmark.com/2009/01/cross-platform-file-locking-support-in-python/).
The codes were released under the BSD License, as is this modified version.

Both implementations do suffer from the stale lock problem.
If process holding the lock dies and lock isn't released,
and stale lock is left in place. Removing it would require manual release
That's something what I really can't accept. Fixing system state
manually absolutely kills productivity and causes constant frustration.

This  problems is created by the fact, that the standard Python file open
function doesn't support any kind file locking at all.

This version is for Windows platform only and solves the stale lock issues 
seen with many implementations by holding the lock file open as long as 
the lock is held.

(C) Sami Lehtinen 2013 / [www.sami-lehtinen.net](http://www.sami-lehtinen.net/)

Any feedback & improvement suggestions are welcome. Scope of this project
is very restricted on purpose. This does exactly and only what it is supposed
to do. Provide simple and reliable file locking on NT/Windows platform.

[WinFileLock Project @ BitBucket](https://bitbucket.org/SamiLehtinen/winfilelock)

KW: Windows, Python, file locking, filelock, filelocking, atomic
safe, stale, non-blocking, solution, code, recipe, github, snippet,
acquire, release, locked, locking mechanism, file mutex, blocking,
lockfile, Windows, NT, Server, instance, library, module, class, 
pure python, without additional libraries, dll, dlls, exclusive
exclusion, operating system, os, platform.
