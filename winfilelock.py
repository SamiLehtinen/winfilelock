"""
WinFileLock
===========

Simple Python 3 file locking for Windows platform only.

This version is for Windows platform only and solves the stale lock issues 
see wiht many implementations by hodling the lock file open as long as 
the lock is held.

(C) Sami Lehtinen 2013 / www.sami-lehtinen.net

Please see: README.md

Project home: https://bitbucket.org/SamiLehtinen/winfilelock

"""

import os
import sys
import time
import errno
import platform
 
class FileLock(object):
    
    class FileLockException(Exception):
        pass
        
    def __init__(self, lock_name, timeout=None, delay=1):
    
        # Make sure this code isn't run on wrong platform
        if not ( os.name == 'nt' and platform.system() == 'Windows' ):
            raise NotImplementedError( \
                  'Platform is not supported.' + \
                  'This code works only on NT/Windows platform.' )
        self.is_locked = False
        self.filename = lock_name
        self.timeout = timeout
        self.delay = delay
        self.file_descriptor = None
            
    def locked(self):
        return self.is_locked
     
    def acquire(self, blocking=True):
        start_time = time.time()
        while True:
            try:
                self.file_descriptor = os.open( self.filename, os.O_CREAT | os.O_EXCL | os.O_RDWR )
                break 
            except OSError as oe:
                try:
                  os.remove( self.filename ) 
                except WindowsError as we:
                    if we.errno == errno.EACCES:
                        if self.timeout is not None and ( time.time() - start_time ) >= self.timeout:
                            raise FileLock.FileLockException( 'Timeout occurred.' )
                        if not blocking:
                            return False
                        time.sleep( self.delay )
                        continue
                    if not we.errno == errno.ENOENT:
                        raise
                if oe.errno != errno.EEXIST:
                    raise
        self.is_locked = True
        return True
 
    def release(self):
        os.close( self.file_descriptor )
        try:
            os.remove( self.filename )
        except WindowsError:
            pass
        self.is_locked = False

    def __enter__(self):
        self.acquire()
        return self
 
    #def __exit__(self, type, value, traceback):
    #    self.release()
 
    def __del__(self):
        if self.is_locked:
            self.release()
    
if __name__ == "__main__":
    # Self check & demo code
    print( 'Create stale lock' )
    with open( 'sample_lock', 'w' ) as f:
        pass
    print( 'First lock object' )
    fl1=FileLock( 'sample_lock' )
    print( 'Second lock object (same resource)' )
    fl2=FileLock( 'sample_lock', timeout=0.1, delay=0.01 )
    print( 'Acquire lock & assert success' )
    assert fl1.acquire() == True
    print( 'Try to acquire lock & assert non blocking failure' )
    assert fl2.acquire( blocking = False ) == False
    print( 'Try to acquire blocking lock, requires FileLockException to exit' )
    try:
        fl2.acquire( blocking = True )
    except FileLock.FileLockException as e:
        pass
    else:
        raise AssertationError( 'FileLockException not raised' )
    print( 'Release lock' )
    fl1.release()
    print( 'Acquire lock & assert success' )
    assert fl2.acquire() == True
    print( 'Release lock' )
    fl2.release()
    print( 'Light self-check passed' )
